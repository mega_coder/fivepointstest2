var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    login: { type: String, index: { unique: true } },
    password: { type: String },
}, { collection: "UserCollection" });

module.exports = mongoose.model("User", UserSchema);