var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubjectSchema = Schema({
    titre: { type: String, index: { unique: true } },
    description: { type: String },
    nbvotes : { type : Number }
}, { collection: "SubjectCollection" });

module.exports = mongoose.model("Subject", SubjectSchema);