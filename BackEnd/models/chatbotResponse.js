var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chatbotResponseSchema = Schema({
    responseText: { type: String },
}, { collection: "chatbotResponseCustom" });

module.exports = mongoose.model("ChatbotResponse", chatbotResponseSchema);