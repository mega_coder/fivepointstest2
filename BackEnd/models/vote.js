var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VoteSchema = Schema({
    user: { type: String, index: { unique: true } },
    subject: { type: String },
    Date : { type: date }
}, { collection: "VoteCollection" });

module.exports = mongoose.model("Vote", VoteSchema);