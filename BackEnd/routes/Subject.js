var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Subject = require('../models/subject');


router.get('/', function (req, res, next) {
    Subject.find().exec(function (err, intents) {
        if (err) {
            res.send(err);
        }
        if (!intents) {
            res.status(404).send();
        }
        else {
            return res.json(intents);
        }
    });
});

router.get('/:id',function (req, res, next) {
    Subject.findOne({_id : req.params.id}).exec(function (err, intents) {
        if (err) {
            res.send(err);
        }
        if (!intents) {
            res.status(404).send();
        }
        else {
            return res.json(intents);
        }
    });
});

module.exports = router;
